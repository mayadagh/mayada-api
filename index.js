const express = require('express');
const app = express();
const cors = require('cors');
var bodyParser = require('body-parser')
var fetch = require('node-fetch');
// mongoose
const mongoose = require('mongoose');
mongoose.set('useCreateIndex',true);
 mongoose.connect('mongodb://interview:mFcibySNx9SdpVa@ds147440.mlab.com:47440/interview-task', 
 { useNewUrlParser: true });

 app.use('/css',express.static(__dirname+'/nodemodule/dist/bootstrap'))
 app.use('/public',express.static(__dirname+'/public'))
  
  
 
 app.use(express.urlencoded({extended: true}));
 
 // parse application/json
 app.use(bodyParser.json())
app.use(express.json()); 
// settings 
app.set('view engine','ejs');

app.use(cors());
app.options('*', cors());

// app.all('*', function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "X-Requested-With");
//     res.header('Access-Control-Allow-Headers', 'Content-Type');
//     next();
// });
  
const permissionAPI = require('./routes/Permission-Task');
app.use('/action', permissionAPI);

const ViewAPI = require('./routes/View-Task');
app.use('/view', ViewAPI);
const roleAPI = require('./routes/Role-Task');
app.use('/role', roleAPI);

 
 

//////////

app.listen(3100);