const express=require('express');
const router = express.Router();
const roleModel=require('../models/role');
 
const PermissionModel=require('../models/permission');
const viewModel=require('../models/view');





 
// 1- List
router.get('/',async (req,res,next)=>{
    var role = await roleModel.find();
      res.render('role',{role:role});
   });
  // 3- show get /role/:id
router.get('/:id',async (req,res,next)=>{
    try{
      var role = await roleModel.findOne({_id: req.params.id});
      if(role){
        res.render('show-role',{role:role});

        
      }else{
        res.status(404).send({error: "role role not found"})
      }
      
    }catch(err){
      res.status(400).send({error: err.message});
    }
  });
  router.get('/rol/add',async (req,res)=>{
    var perm = await PermissionModel.find({}).select(["-IsGlobal","-__v"]);
    var view = await viewModel.find({}).select(["-action","-__v"]);

    
console.log(perm);
console.log(view);

    res.render('add-role',{perm:perm,view:view});
  
  }); 
// 2- create role /role
router.post('/',async (req,res,next)=>{
    var new_role = new roleModel(req.body);
    try{
      if(new_role.roleName==null||new_role.viewName==null||new_role.action==null)
      {
        return;
      }
      var role = await new_role.save();
      res.redirect('/role');
    }catch(err){
      res.status(400).send({error: err.message});
    }
  });
  
// 4- update put /role/:id
router.put('/:id',async (req,res,next)=>{
    try{
      var role = await roleModel.findByIdAndUpdate(req.params.id, req.body);
      if(role){ 
 
        res.render('role',{role:role});
      }else{
        res.status(404).send({error: "role not found"})
      }
    }catch(err){
      res.status(400).send({error: err.message});
    }
  });
// 5- delete delete /role/:id
router.delete('/:id',async (req,res,next)=>{
    try{
      var result = await roleModel.delete({_id: req.params.id});
      if(result){
        res.redirect('role');
      }else{
        res.status(404).send({error: "role not found"})
      }
    }catch(err){
      res.status(400).send({error: err.message});
    }
  });
  module.exports = router;

